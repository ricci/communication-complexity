\documentclass{tufte-handout} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)
\usepackage{geometry} % to change the page dimensions

\usepackage{graphicx} 
\usepackage{amsmath,amsthm,url}
\newtheorem{lemma}{Lemma}
\newtheorem{definition}{Definition}
\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim

\usepackage[draft,inline,nomargin,index]{fixme}
\fxsetup{theme=colorsig,mode=multiuser,inlineface=\sffamily,envface=\itshape}
\FXRegisterAuthor{su}{asu}{Suresh}
\FXRegisterAuthor{ro}{aro}{Robert}


\newcommand{\determin}[1]{\mathcal{D}\left(#1\right)}
\newcommand{\randR}[1]{\mathcal{R}\!\left(#1\right)}
\newcommand{\equ}{\mathcal{E\!Q}}
\newcommand{\gtfunc}{\mathcal{GT}}
\newcommand{\disjoint}{\mathcal{DISJ}}
\newcommand{\mP}{\mathcal{P}}
\newcommand{\nexists}{\not{\exists}}
%%% END Article customizations

%%% The "real" document content comes below...

\title{CS 7931: Lecture 3}
\author{Robert Christensen}
%\date{September 10, 2014}

\begin{document}
\maketitle

% Why does the size matter with median computations?
%
%The rational that you remove the same amount from both sides
%only matters if it removes the same fraction at each iteration.
%Equality is necessary $|n_{\text{Alice}}| \equal |n_{\text{Bob}}|$,
%almost equal does not work.

% recording ~3:00
\section{Randomized Algorithm Trick Review}

We will review the trick we used to solve $\equ$ quickly,
but we have some small probability $\epsilon$ the the
answer returned in not correct.  This trick shows up
often enough in communication complexity that it is 
worth reviewing.

We start with $X$ and $Y$ and $n$ bits strings.  We pick
a value for $p$ which is some prime number which will define
the field.  We will explore how the value of $p$ effects
the probability later, so we will leave the value of $p$
undefined at this time.

We convert the strings to a field $GF(p)$  where the polynomial
is defined as

\begin{align*}
X(\omega) &= \sum x_i \omega^i \\
Y(\omega) &= \sum y_i \omega^i
\end{align*}

Basically, Alice evaluates the polynomial at a random
location and sends that location and the result at that
location to Bob.  Bob checks to see if his polynomial 
has the same value at that point and replies if it
is the same or different.  The protocol can be expressed
using a more graphical representation.

\vspace{2em}

\begin{tabular}{r l c r l}
A: & $r\in_R F_p$&               &    &        \\
A: & $X(r)$, $r$ & $\rightarrow$ & B: & $Y(r)$ \\
   &             &               & B: & check if $X(r) = Y(r)$
\end{tabular}

\paragraph{Why is this correct?}  We can easily see that if the two
are equal that it will return true because evaluating
the polynomial at the same spot will return the same
result, so Bob will report they are equal.

% recording ~7:30
If they are different we are basically asking what the
probability that $X(r) = Y(r)$ even though $x\neq y$
where $r\in_R F_p$.  We know from last lecture that
two polynomials which are different and of degree $d$ can
only agree in at most $d$ points.  The polynomials $X$ and
$Y$ have degree $n$, so they can only agree in up to $n$ points.
Since the field is of size $p$, there are only $p$ distinct locations
where $X$ and $Y$ could intersect.  Thus, the probability of failure
for this protocol is

\begin{equation*}
Pr(X(r) = Y(r) | x\neq y) \sim \frac{n}{p}
\end{equation*}

We can adjust the probability of failure by scaling the
value of $p$.  If $p \sim n^2$ we have a very small probability
of failure.  If we want the probability to be about $1/3$, we can
find a prime number for $p \sim 3n$\footnote{A prime number will exist in
that range by a well known result in number theory.}.

Now that we know why it works and how much information must
be communicated, we figure out how much communication cost
this has.  Because we must send the value of $r$ and the value
of $X(r)$ we will need $\sim \log p$ bits.

This is one of the rare examples where we can provable show
that randomness can improve a result exponentially.

% Communication complexity is most often used to find lower bounds,
% but in this case we found an upper bound.

\section{Using the Results of $\equ$ to Find $\gtfunc$ Using Randomness}

How can we use what we know now to create a solution for $\gtfunc$ if
we assume the imputes $x$ and $y$ a binary representations of unsigned
integers? The function $\gtfunc$ is defined as

\begin{equation*}
\gtfunc(x,y) = \left\{
  \begin{array}{l l}
    1 & \text{if $x > y$} \\
    0 & \text{otherwise}
  \end{array} \right.
\end{equation*}

% We can't convert $x$ and $y$ to be a polynomial $X$ and $Y$ and check at 
% some random point if $X(r)>Y(r)$ similar to what we did for $\equ$.  We also
% can not send a single bit accross and check if the differ, because only a single
% bit needs to differ for $\gtfunc$ to be true.

One observation which will lead to a solution is that if $x=y$ we know
$\gtfunc(x,y) = 0$.  When $x \neq y$ we only have to detect the leftmost position $i$
where $X_i \otimes Y_i = 1$ (the most significant bit which differs between 
the two inputs).  If we compare the bits at $X_i$ and $Y_i$ we can use the following
table to give the solution.

\begin{equation}
\label{equ:gtfunction}
\gtfunc(x,y) = \left\{
  \begin{array}{l l}
  0 & \text{if there is no such $i$ where $X_i \otimes Y_i = 1$} \\
  1 & \text{if $X_i = 1$ and $Y_i = 0$} \\
  0 & \text{if $X_i = 0$ and $Y_i = 1$}
  \end{array} \right.
\end{equation}

Where the value of $i$ in (\ref{equ:gtfunction}) is defined in the previous paragraph.
Using this technique we can return the correct result of $\gtfunc$ as long as we can
find the leftmost differing bit.  Since we know that all the bits left of $i$ are equal
\footnote{If they were not equal it would mean there is some bit which differs in that
region.} we can use the randomized solution of $\equ$ to find the first differing bit
by doing a binary search.

The protocol will run as follows,  Run $\equ$ on the \emph{left bits}.  If $\equ$ returns that
$x\neq y$ we know that the first differing bit is in the left region which we just checked.
If $\equ$ returns that $x = y$, if a differing bit exists it must exist in `right bits' which
we have not checked yet.  We keep adjusting our binary search range until we find the most 
significant bit which differs or we detect that $x=y$.

What is the communication complexity of $\gtfunc$?  Since we are doing a binary search,
we will have $O(\log n)$ iteration.  Each iteration will be running the randomized $\equ$
which has a communication complexity of $O(\log n)$ as well.  Thus, the communication
complexity for the randomized $\gtfunc$ is

\begin{equation*}
\mathcal{R}_{\epsilon}\!\left(\gtfunc \right) \leq O( \log^2 n)
\end{equation*}

\paragraph{private vs public randomness}  There are three ways 
we can define random protocols which we have studied thus far.
Currently we are being charged for the randomness in the protocol
because if we want our randomness to be known to the other it must
be communicated.  What if we want to see how effective of our random
algorithm?  We can see how effective the algorithm is by trying the
algorithm with and without paying for the cost of communicating
randomness and see how much is spend for randomness.

We have been using a \emph{private coin model} where Alice and Bob have
a private source of randomness.  The \emph{public coin model} is such that
both Alice and Bob share the randomness without paying a penalty.  One could
think of it as Alice and Bob writing their random bits on a white board which
is seen by both Alice and Bob (while the information $x$ and $y$ remain private
and still cost to be communicated).

\begin{marginfigure}
\centering
\includegraphics{figs/random_AliceBob}
\end{marginfigure}

Shared randomness can provide a powerful mechanism to solve the problems
which we have already explored.  Using the \emph{public coin model} how much
can we improved on the communication complexity of $\equ$?  By changing from
\emph{public} to \emph{private} we can reduce the communication complexity to
$2$ bits.

To do this, Alice and Bob select $n$ random bits together.  Using the same setup
and definition of the polynomials as we described in the beginning of the
lecture.

\vspace{2em}

\begin{tabular}{r l c r l}
A: & $x \cdot r$ & $\rightarrow$ & B: & calculate $y \cdot r$ \\
   &               &             & B: & check if  $x \cdot r = y \cdot r$
\end{tabular}

\vspace{2em}

Sending $x \cdot r$ will only take a single bit and returning the answer
will take a single bit, so the total cost will be $2$.  Since we are only
sending $x \cdot y$ over $GF(2)$ we will fail with probability $1/2$.  To
get a smaller probability of failure we repeat with a different value for $r$.
If we repeat the experiment twice we will have a probability of failure of
$1/4$, which is better then our arbitrary threshold of $1/3$.


\section{Converting Between Public and Private Randomness}

Lets looks at the cost comparison between public cost and private
cost.

\begin{equation*}
\mathcal{R}_{\epsilon}^{\text{pub}}(f) \le \mathcal{R}_{\epsilon}(f)
\end{equation*}

When one converts from a public randomness and private randomness,
a small additive penalty is added to the communication
complexity.

\begin{theorem}
\begin{equation*}
\mathcal{R}_{\epsilon + \delta}(f) \le \mathcal{R}_{\epsilon}^{\text{pub}}(f) + O\left(\log n \log \frac{1}{\delta}\right)
\end{equation*}

where $\delta$ is some parameter $<1$ and $\epsilon$ is the error probability.
\end{theorem}

For problems with $O(n)$ it might not matter significantly.  For $O(\log n)$
it might differ more.  We can use this to create public communication methods.
The reason why this is useful is because it is more easy to understand the
algorithm with public randomness, so we convert to public randomness to more
easily reason about the problem and convert back when it is necessary.

The trivial way to convert a public coin protocol to a private coin protocol is
to just sent all the random bits.  However, using the naive conversion does not
always result in a very satisfying conversion (think about the difference between
the private coin version of $\equ$ which takes $O(\log n)$ and the public
coin version which takes $O(1)$).  We will convert the public coin protocol
to a new public coin protocol which uses significantly fewer bits which require
sharing (with the knowledge that sharing the random bits does not require a
communication penalty).

% recording 42:20
The trick we will use is to change the way randomness is used in the
protocol.  Alice and Bob will agree on a series of strings $r_1 \ldots r_t$.
Each string is several bits long and $t$ strings will exist.  The sequence $r_1 \ldots r_t$
are created and written down before Alice or Bob look at their input.
Alice and Bob will fix one of $r_1 \ldots r_t$ and use that for randomness.  It takes
$\log t$ bits of pure randomness o index one of the fixed strings in $r_1 \ldots r_t$.
We only change the randomness  to select $r_1 \ldots r_t$, which takes $\log t$ to
select the sequence to use.

This is the same regardless of the protocol.  The only difference is the randomness
used to select the element from $r_1 \ldots r_t$.  This takes a maximum of $\log t$
bits to index the sequence.

This is how we will create our new public coin protocol.  If we hard wire the strings
$r_1 \ldots r_t$ for Alice and Bob we no longer have to communicate the strings thus
removing the communication penalty from he cost of using this method.

How can we prove this works if we want

\begin{align*}
\log t & \approx \log n + \log \frac{1}{\delta} \\
t &\approx n \frac{1}{\delta}^c
\end{align*}

The proof involves a Chernoff bound.  First we define a random
variable $Z$ with the property

\begin{equation*}
Z(x,y,r) = \left\{
  \begin{array}{l l}
    1 & \text{if the answer is wrong} \\
    0 & \text{otherwise}
  \end{array} \right.
\end{equation*}

By the definition that a public coin protocol has
an error $\epsilon$

\begin{equation*}
\operatorname*{E[Z]}_{r \in R} \le \epsilon
\end{equation*}

Next, we choose $r_1 \ldots r_t \in_R R$ at random
and by the Chernoff inequality we get

\begin{equation}
\label{equ:randChern}
Pr\left[ \frac{1}{t} \sum_{i=1}^t Z(x,y,r_i) \ge \epsilon + \delta \right] \le 2 e^{-2 \delta^2 t}
\end{equation}

Where the right had side $\le 2 e^{-2 \delta^2 t}$ comes from applying
the random Chernoff bound to the probability.

We want to say it will work for $\forall x,y$.  Since (\ref{equ:randChern}) is
he probability of error of a single $x,y$ pair, the probability of failure for
$\forall x,y$ is calculated by taking the product of all possible probabilities.
The probability of failure for a single item is $2 e^{-2\delta^2 t}$.  There
exits a total of $2^{2n}$ possible combinations.  The product of the two
gives the total probability of failure.

\begin{equation}
\label{equ:randProbFail}
\text{Prob of Failure} = 2^{2n} 2 e^{-2\delta^2t} < 1
\end{equation}

We want (\ref{equ:randProbFail}) to be $<1$ because that means there
must exists some sequence of $r_1 \ldots r_t$ that works for $\forall x$
because the probability is $<1$.

If we solve (\ref{equ:randProbFail}) we get $t \sim \Theta \left( n / \delta^2 \right)$

We have just proved that for every private coin protocol there exists some public
coin protocol that is not much worse.  How one
actually finds the protocol is a different topic, but we now know that one
exists and can be found.

%Why do we want to convert between public and private?
%
%\begin{enumerate}
% \item Because public coin protocol is more simple to understand
% 
% \item A technical tool for using probabilistic methods.
%\end{enumerate}

\section{How Does a Randomized Algorithm Compare to the Deterministic Algorithm?}

If we have shown a randomized algorithm to do exponentially better than a 
deterministic algorithm.  Is is possible for a randomized algorithm to do
even better?  The answer is no, the randomized algorithm is bounded.

\begin{lemma}
$
\mathcal{R}_{\epsilon}(f) \ge \Omega\left( \log \determin{f} \right)
$
\end{lemma}

To prove this we take a randomized algorithm and convert it to be
a deterministic algorithm.  We can not assume anything about the function,
we can only reason about the protocol.

Alice can run a protocol as many times as she would like.  Take a particular
leaf in the randomized protocol; what is the probability we will end up
at this leaf?

The path is well defined.  Send the probability to Bob $P_A(\ell)$.  Bob
does the same and returns the value.  The probability of arriving
at a given leaf is $P_A(\ell) P_B(\ell)$.  Continue to collect the probability
until a leaf node is reached.  Bob can now calculate the probability.

The number of leafs using this method is $2^{\mathcal{R}_\epsilon(t)}$.  If 
we say $b$ is the bit complexity at each leaf, the total deterministic
complexity will be

\begin{equation*}
\determin{t} \le 2^{\mathcal{R}_\epsilon(t)} b
\end{equation*}

Remember, to find the answer deterministically find the answer, all the leaves
in the protocol must be explored because the probability must be calculated
internally in order to find which one is the solution deterministically.

This is basically a brute for solution.  This has not showed us what the best
we can do using randomized algorithms.  Proving the best we can do is difficult
and will be discussed in the future.  Proving the complexity of deterministic
algorithm with random inputs will help prove the complexity of randomized algorithms.

%\newpage

%\bibliographystyle{plain}
%\bibliography{L2}

\end{document}
